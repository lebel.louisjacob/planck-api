#!/usr/bin/env python3
from math import sqrt, pi as PI
from time import time as secs_since_epoch

class PlanckTime():
    PLANCKS_IN_A_SEC = int(5.39124760*(10**44))
    PLANCKS_IN_A_NANO = PLANCKS_IN_A_SEC * (10**9)

    YEARS_UNTIL_EPOCH = int(13.8 * (10**9))
    DAYS_IN_YEAR = 365.25
    SECS_UNTIL_EPOCH = YEARS_UNTIL_EPOCH * DAYS_IN_YEAR * 24 * 60 * 60
    NANOS_UNTIL_EPOCH = int(SECS_UNTIL_EPOCH * (10**9))

    def nanos_since_epoch(self):
        return int(secs_since_epoch() * (10**9))

    def now(self):
        nanos_until_now = PlanckTime.NANOS_UNTIL_EPOCH + self.nanos_since_epoch()
        plancks_until_now = nanos_until_now * PlanckTime.PLANCKS_IN_A_NANO
        return plancks_until_now

time = PlanckTime()
